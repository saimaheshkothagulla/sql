
CREATE DATABASE College;
USE College
CREATE TABLE Classes(
	  ID INT PRIMARY KEY IDENTITY(1,1),
	  Name VARCHAR(10) NOT NULL,
	  Section CHAR NOT NULL,
	  Number INT NOT NULL

);

CREATE TABLE Teachers(
    ID INT PRIMARY KEY IDENTITY(1,1),
	Name VARCHAR(50) NOT NULL,
	DOB DATE NOT NULL,
	Gender VARCHAR(15) NOT NULL

);
INSERT INTO Classes values ('IX','A',201)
INSERT INTO Classes values ('IX','B',202)
INSERT INTO Classes values ('X','A',203)

INSERT INTO Teachers values ('Lisa Kudrow','1985/06/08','Female')
INSERT INTO Teachers values ('Monica Bing','1982/03/06','Female')
INSERT INTO Teachers values ('Chandler Bing','1978/12/17','Male')
INSERT INTO Teachers values ('Ross Geller','1993/01/26','Male')

CREATE TABLE Students(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Name VARCHAR(50) NOT NULL,
	DOB DATE NOT NULL,
	Gender VARCHAR(10) NOT NULL,
	ClassId INT NOT NULL
);
INSERT INTO Students values ('Scotty Loman','2006/01/31','Male',1)
INSERT INTO Students values ('Adam Scott','2005/06/01','Male',1)
INSERT INTO Students values ('Natosha Beckles','2005/01/23','Female',2)
INSERT INTO Students values ('Lilly Page','2006/11/26','Female',2)
INSERT INTO Students values ('John Freeman','2006/06/14','Male',2)
INSERT INTO Students values ('Morgan Scott','2005/05/18','Male',3)
INSERT INTO Students values ('Codi Gass','2005/12/24','Female',3)
INSERT INTO Students values ('Nick Roll','2005/12/24','Male',3)
INSERT INTO Students values ('Dave Grohl','2005/02/12','Male',3)

SELECT * FROM Students;

SELECT * FROM Teachers

SELECT * FROM Classes;




CREATE TABLE TeacherClassMapping(
	Id INT PRIMARY KEY IDENTITY(1,1),
	teacherID INT NOT NULL,
	classID INT NOT NULL,
	FOREIGN KEY(teacherID) REFERENCES Teachers(Id),
	FOREIGN KEY(classID) REFERENCES Classes(Id)
);
insert into TeacherClassMapping values(1,1),(1,2),(2,2),(2,3),(3,3),(3,1)

--Find list of male students
SELECT * FROM Students 
WHERE Gender='Male'
--Find list of student older than 2005/01/01
SELECT * FROM Students 
WHERE DOB<'2005-01-01'

--Youngest student in school


	SELECT TOP 1 * 
	FROM Students ORDER BY DOB;

--Find student distinct birthdays
	SELECT DISTINCT DOB 
	FROM Students 
-- No of students in each class

	SELECT ClassId,COUNT(*) as [Count of students] 
	FROM Students 
	GROUP BY ClassId

-- No of students in each section

	SELECT C.Section,count(*) AS [NO OF STUDENTS] 
	FROM Classes C 
	INNER JOIN Students S 
	ON C.ID=S.ClassId 
	GROUP BY C.Section

-- No of classes taught by teacher--
   SELECT T.Name,COUNT(*) AS [NO OF CLASSES]
   FROM TeacherClassMapping TCM
   INNER JOIN Teachers T
   ON TCM.teacherID=T.ID
   GROUP BY T.Name;
-- List of teachers teaching Class X

	SELECT T.Name AS [List of teachers teaching Class X] 
	FROM Teachers T 
	INNER JOIN TeacherClassMapping TCM 
	ON T.ID=TCM.teacherID 
	INNER JOIN Classes C 
	ON TCM.classID=C.ID 
	WHERE C.Name='X'
-- Classes which have more than 2 teachers teaching

	SELECT classID,COUNT(*) AS [NO OF TEACHERS TEACHING] 
	FROM TeacherClassMapping  
	GROUP BY classID
	having COUNT(*)>2
-- List of students being taught by 'Lisa'
	SELECT S.Name AS [Student Names]
	FROM Students S
	INNER JOIN TeacherClassMapping TCM
	ON S.ClassId=TCM.classID 
	INNER JOIN Teachers T 
	ON T.ID=TCM.teacherID 
	WHERE T.Name like 'Lisa%'