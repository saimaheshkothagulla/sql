CREATE DATABASE movies
use movies

CREATE TABLE Actors(
	Id int PRIMARY KEY IDENTITY(1,1),
	Name VARCHAR(45) NOT NULL,
	Gender VARCHAR(15) NOT NULL,
	DOB DATE NOT NULL   
)

CREATE TABLE Producers(
	Id int PRIMARY KEY IDENTITY(1,1),
	Name VARCHAR(45) NOT NULL,
	Company VARCHAR(30) NOT NULL,
	CompanyEstDate DATE NOT NULL

)
CREATE TABLE Movies(
	Id int PRIMARY KEY IDENTITY(1,1),
	Name VARCHAR(45) NOT NULL,
	Language VARCHAR(15) NOT NULL,
	ProducerID int NOT NULL,
	Profit int NOT NULL,
	FOREIGN KEY(ProducerID) REFERENCES Producers(Id)
)


CREATE TABLE ActorMovieRelationship(
	Id INT PRIMARY KEY IDENTITY(1,1),
	movieID int NOT NULL,
	actorID int NOT NULL,
	FOREIGN KEY(movieID) REFERENCES Movies(Id),
	FOREIGN KEY(actorID) REFERENCES Actors(Id)
)

INSERT INTO Actors VALUES('Mila Kunis','Female','11/14/1986')
INSERT INTO Actors VALUES('Robert DeNiro','Male','07/10/1957')
INSERT INTO Actors VALUES('George Michael','Male','11/23/1978')
INSERT INTO Actors VALUES('Mike Scott','Male','08/04/1969')
INSERT INTO Actors VALUES('Pam Halpet','Female','09/26/1996')
INSERT INTO Actors VALUES('Dame Judi Dench','Female','04/05/1947')

SELECT * FROM Actors

INSERT INTO Producers VALUES('Arjun','Fox','05/14/1998'),
							('Arun','Bull','09/11/2004'),
							('Tom','Hanks','11/03/1987'),
							('Zeshan','Male','11/14/1996'),
							('Nicole','Team Coco','09/26/1992')
SELECT * FROM Producers


INSERT INTO Movies VALUES('Rocky','English',1,10000),
						('Rocky','Hindi',3,3000),
						('Terminal','English',4,300000),
						('Rambo','Hindi',2,93000),
						('Rudy','English',5,9600)

SELECT * FROM Movies

INSERT INTO ActorMovieRelationship VALUES(1,1),(1,3),(1,5),
										 (2,6),(2,5),(2,4),(2,2),
										 (3,3),(3,2),
										 (4,1),(4,6),(4,3),
										 (5,2),(5,5),(5,3)



--1.Update Profit of all the movies by +1000 where producer name contains 'run'
UPDATE Movies SET Profit=Profit+1000 WHERE ProducerID=
(SELECT Id FROM Producers where Name LIKE '%run%')
SELECT * FROM Movies

--2.Find duplicate movies having the same name and their count
	SELECT Name,COUNT(*) AS [DUBLICATION COUNT] 
	FROM Movies 
	GROUP BY Name 
	HAVING COUNT(*)>1

--3.Find the oldest actor/actress for each movie

	SELECT A1.id,A.Name,A.DOB FROM Actors A
	INNER JOIN(
	SELECT m.Id ,MIN(a.DOB) AS DOB FROM Movies M 
	INNER JOIN ActorMovieRelationship AMP ON M.Id=AMP.movieID
	INNER JOIN Actors A  ON A.Id=AMP.actorID 
	GROUP BY M.Id
	) AS A1 ON A.DOB=A1.DOB  
	ORDER BY A1.Id


--4.List of producers who have not worked with actor X

	  SELECT P.Id,P.Name FROM Producers P
	  Where P.Id NOT IN (
	    SELECT P.Id FROM Producers P
	  INNER JOIN Movies M ON M.ProducerID=P.Id 
	  INNER JOIN ActorMovieRelationship AMR ON M.Id=AMR.movieID
	  INNER JOIN Actors A ON A.Id=AMR.actorID
	  WHERE A.Id=1
	  
	  )

--5.List of pair of actors who have worked together in more than 2 movies
   SELECT A.Name AS ACTOR1,A1.Name as ACTOR2 ,COUNT(AMR.movieID) AS [no of times both acted on same movies]
   FROM Actors A 
   INNER JOIN ActorMovieRelationship AMR ON A.Id=AMR.actorID
   INNER JOIN ActorMovieRelationship AMR1 ON AMR1.movieID=AMR.movieID
   INNER JOIN Actors A1 ON A1.Id=AMR1.actorID  
   WHERE A.Id<A1.Id
   GROUP BY A.Name,A1.Name
   HAVING COUNT(AMR.movieID)>=2 AND A.Name<>A1.Name


--6.Add non-clustered index on profit column of movies table
  CREATE NONCLUSTERED INDEX [Profit Column]
  ON Movies(Profit)
 
  SELECT * FROM Movies WHERE profit=94000
--7.Create stored procedure to return list of actors for given movie id

       --return list of actors for given movie id without stored  procedure
	     SELECT A.Id AS ACTOR_ID,A.Name AS ACTOR_NAME  FROM Movies M 
		 INNER JOIN ActorMovieRelationship AMR ON M.Id=AMR.movieID
		 INNER JOIN Actors A ON A.Id=AMR.actorID
		 WHERE M.Id=1

       --return list of actors for given movie id with stored  procedure
	   GO
	   CREATE PROCEDURE ListOfActorsOfGivenMovieId(@MovieId INT)
	   AS
			
	     SELECT A.Id AS ACTOR_ID,A.Name AS ACTOR_NAME  
		 FROM Movies M
		 INNER JOIN ActorMovieRelationship AMR ON M.Id=AMR.movieID
		 INNER JOIN Actors A ON A.Id=AMR.actorID
		 WHERE M.Id=@MovieId
	 
	   EXEC ListOfActorsOfGivenMovieId 1


--8.Create a function to return age for given date of birth
	GO
	CREATE FUNCTION Age(@DOB AS DATE)
	RETURNS INT
	AS
	BEGIN
		DECLARE @age INT
		SET @age = DATEDIFF(YEAR,@DOB,GETDATE())
		RETURN @age
	END
	SELECT dbo.Age('1999-11-29') AS PersonAge
--9.Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated) 

     GO
	 CREATE PROCEDURE IncreasIngProfitBy100(@movieIds VARCHAR(20))
	 AS
	 UPDATE  movies SET Profit+=100 WHERE Id IN (
	 SELECT * FROM string_split(@movieIds,',')

	 )
	 EXEC IncreasIngProfitBy100 '1,2,3'

	 SELECT * FROM Movies